import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {AppAccessdeniedComponent} from "./core/pages/app.accessdenied.component";
import {AppNotfoundComponent} from "./core/pages/app.notfound.component";

const routes: Routes = [
    { path: '', loadChildren: () => import('../app/dashboard/dashboard.module').then(m => m.DashboardModule) },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

    { path: 'nao-autorizado', component: AppAccessdeniedComponent },
    { path: 'pagina-nao-encontrada', component: AppNotfoundComponent },
    { path: '**', redirectTo: 'pagina-nao-encontrada' }
  ];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
