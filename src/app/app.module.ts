import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from "./core/core.module";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {MessageModule} from "primeng/message";
import {MessagesModule} from "primeng/messages";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        // BrowserModule.withServerTransition({appId: 'serverApp'}),
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ToastModule,
        MessageModule,
        MessagesModule,

        CoreModule,
        AppRoutingModule
    ],
    providers: [MessageService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
