import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DashboardComponent} from "../../../dashboard/dashboard.component";

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html',
    styleUrls: ['./app.menu.component.scss']
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(
        public app: DashboardComponent,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.model = [
            {label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/']},
 
            {
                label: 'Pesquisa',
                icon: 'pi pi-fw pi-cog',
                routerLink: ['/lote/pesquisa'],
            }
        ];
    }

    onMenuClick() {
        this.app.menuClick = false;
    }

}
