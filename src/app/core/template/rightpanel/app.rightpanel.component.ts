import {Component, Inject, forwardRef} from '@angular/core';
import { DashboardComponent } from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-rightpanel',
	templateUrl: './app.rightpanel.component.html',
	styleUrls: ['./app.rightpanel.component.scss']
})

export class AppRightPanelComponent {
	today: string;

    constructor(public app: DashboardComponent) {
		this.today = this.getDate();
	}

	getDate() : string {
		let dt = new Date();

		let month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
		let week  = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];

		let semana = week[dt.getDay()];
		let mes = month[dt.getMonth()];

		return semana + ', ' + dt.getDate() + ' de ' + mes + ', ' + dt.getFullYear();
	}
}

