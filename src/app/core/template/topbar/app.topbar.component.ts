import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {DashboardComponent} from "../../../dashboard/dashboard.component";

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {

    activeItem: number;
    currentUser: any;

    constructor(
        public app: DashboardComponent,
        private router: Router
    ) {

    }

    mobileMegaMenuItemClick(index) {
        this.app.megaMenuMobileClick = true;
        this.activeItem = this.activeItem === index ? null : index;
    }

}
