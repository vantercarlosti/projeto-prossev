import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/core/template/breadcrumb/breadcrumb.service';

@Component({
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Home', routerLink: ['/'] }
    ]);
  }

  ngOnInit() {}

}
