import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {DashboardComponent} from "./dashboard.component";
import {DashboardHomeComponent} from "./dashboard-home/dashboard-home.component";
import {CoreModule} from "../core/core.module";


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        CoreModule
    ],
    declarations: [
        DashboardComponent,
        DashboardHomeComponent
    ],
    providers: [
    ]
})
export class DashboardModule {
}
