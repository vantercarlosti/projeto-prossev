import { Title } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/core/template/breadcrumb/breadcrumb.service';
import { LoteService } from 'src/app/shared/services/lote.service';
import { Lote } from 'src/app/shared/models/lote';

@Component({
  selector: 'app-detalhes-lote',
  templateUrl: './detalhes-lote.component.html',
  styleUrls: ['./detalhes-lote.component.css']
})
export class DetalhesLoteComponent implements OnInit {

  formulario: FormGroup;
  loteSelecionado: any = null;
  pesquisaLote: string;
  displayModal: boolean = false;
  displayModalLote: Boolean = false;

  constructor(
    private loteService: LoteService,
    private route: ActivatedRoute,
    private router: Router,
    private breadcrumbService: BreadcrumbService,
    private title: Title,
    private formBuilder: FormBuilder
  ) { 
    this.breadcrumbService.setItems([
      { label: 'Home', routerLink: ['/'] },
      { label: 'lote', routerLink: ['/lote/pesquisa'] },
      { label: 'Cadastros' }
  ]);
  }

  ngOnInit() {
    this.configurarFormulario();
    const codigoLote = this.route.snapshot.params['id'];

    this.title.setTitle('Novo Lote');

    if (codigoLote) {
      this.carregarLote(codigoLote);
    }

  }

  configurarFormulario() {
    this.formulario = this.formBuilder.group({
      id: [],
      nome: this.formBuilder.control(''),
      lote: this.formBuilder.control(''),
      codigo: this.formBuilder.control(''),
      ean: this.formBuilder.control(''),
      data_validade: this.formBuilder.control(''),
      quantidade: this.formBuilder.control(''),
      quantidade_contada: this.formBuilder.control(''),
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  validarTamanhoMinimo(valor: number) {
    return (input: FormControl) => {
      return (!input.value || input.value.length >= valor) ? null : { tamanhoMinimo: { tamanho: valor } };
    };
  }

  get editando() {
    return Boolean(this.formulario.get('id').value);
  }

  onSelectClient(lote: Lote){
      this.loteSelecionado = lote;
  }


  carregarLote(id: number) {
    this.loteService.buscarPorCodigo(id)
      .then(lote => {
        console.log(lote);
        this.formulario.patchValue(lote);
      })
  }

  showDialog() {
    this.displayModal = true;
  }


  atualizarLote() {
    this.loteService.putLote(this.formulario.value)
      .then(lote => {
        console.log(this.formulario.value)
        this.formulario.patchValue(lote);
      })
  }

  cancelarModal(): void {
    this.displayModal = false;
    this.displayModalLote = false;
  }

}
