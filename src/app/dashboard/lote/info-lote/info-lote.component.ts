import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from 'src/app/core/template/breadcrumb/breadcrumb.service';
import { Lote } from 'src/app/shared/models/lote';
import { LoteService } from 'src/app/shared/services/lote.service';

@Component({
  selector: 'app-info-lote',
  templateUrl: './info-lote.component.html',
  styles: ['./info-lote.component.scss']
})
export class InfoLoteComponent implements OnInit {

  filtro = new Lote();
  loteCod: number;
  lotes = [];
  lote: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loteService: LoteService,
    private breadcrumbService: BreadcrumbService
  ) {
    this.route.params.subscribe(params => {
      const cod = +params.cod;
      if (!!cod && cod > 0) {
        this.loteCod = cod;

        this.breadcrumbService.setItems([
          { label: 'Home', routerLink: ['/'] },
          { label: 'lote', routerLink: ['/lote/pesquisa'] },
          { label: cod.toString() }
        ]);

      }

    });
  }

  ngOnInit(): void {

  }

  atualizarLote(lote: any) {
    this.loteService.putLote(lote)
      .then(lote => {
        console.log(lote)
      })
  }

  consultar() {
    this.loteService.listar(this.filtro)
        .subscribe(resposta => {
            this.lotes = resposta.map(lote => new Lote(lote))
            console.log(this.lotes)
        })
  }

  onKeydownQuantidadeManual(event: KeyboardEvent, tr: HTMLTableRowElement, item: Lote): void {
    let keyCode = event.keyCode;
    let value = (<HTMLInputElement> event.target).value;
    let quantidade: number = 0;

    if(value != null && value.trim() != "") {
        quantidade = parseInt(value);
    }

    switch (keyCode) {
        // enter
        case 13: {
            if(value != null && value.trim() != "") {
                this.alterarQuantidadeItem(item, quantidade);
            }
        }
        // esc
        case 27: {
            break;
        }
        // seta cima
        case 38: {
            if(value != null && value.trim() != "") {
                this.alterarQuantidadeItem(item, quantidade);
            }
            this.previousQuantidadeItem(tr);
            break;
        }
        // tab
        case 9: {
            event.preventDefault();
        }
        //seta baixo
        case 40: {
            if(value != null && value.trim() != "") {
                this.alterarQuantidadeItem(item, quantidade);
            }
            this.nextQuantidadeItem(tr);
            break;
        }
    }
}

private nextQuantidadeItem(tr: HTMLTableRowElement): void {
  let next = <HTMLTableRowElement> tr.nextElementSibling;
  if(next != null) {
      setTimeout(() => {
          this.editarQuantidade(next.querySelector(".quantidade"));
      }, 100);
  }
}

private previousQuantidadeItem(tr: HTMLTableRowElement): void {
  let previous = <HTMLTableRowElement> tr.previousElementSibling;
  if(previous != null) {
      setTimeout(() => {
          this.editarQuantidade(previous.querySelector(".quantidade"));
      }, 100);
  }
}

editarQuantidade(div: HTMLDivElement): void {
  try {
      div.classList.add("edicao");
      setTimeout(() => {
          (<HTMLInputElement> div.lastElementChild.firstElementChild).focus();
      }, 100);
  } catch(e) {
      console.log("Erro ao clicar alterar quantidade: " + e);
  }
}

private alterarQuantidadeItem(item: any, novaQuantidade: number | string) {
  if(typeof novaQuantidade === 'string') {
      novaQuantidade = parseInt(novaQuantidade);
  }

  // this.itemWebsocket.send(<WmsMessage>{
  //     event: Comando.ATUALIZAR_ITEM,
  //     data: {
  //         codigo: item.codigo,
  //         produtoCodigo: item.produtoCodigo,
  //         quantidade: novaQuantidade
  //     },
  //     params: {
  //         usuarioCodigo: user.codigo,
  //         usuarioNome: user.nome
  //     }
  // });
}

}
