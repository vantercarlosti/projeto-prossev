import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { TabViewModule } from 'primeng/tabview';
import { LoteRoutingModule } from './lote.routing.module';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';
import { DialogModule } from 'primeng/dialog';
import { SelectButtonModule } from 'primeng/selectbutton';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { LoteComponent } from './lote.component';
import { PesquisaLoteComponent } from './pesquisa-lote/pesquisa-lote.component';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { PasswordModule } from 'primeng/password';
import { CustomComponetsModule } from "../../shared/custom-componets/custom-componets.module";
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FileUploadModule } from 'primeng/fileupload';
import { LoteService } from 'src/app/shared/services/lote.service';
import { DetalhesLoteComponent } from './detalhes-lote/detalhes-lote.component';
import { CoreModule } from 'src/app/core/core.module';
import { InfoLoteComponent } from './info-lote/info-lote.component';

@NgModule({
    imports: [
        CardModule,
        CommonModule,
        FormsModule,
        DialogModule,
        ReactiveFormsModule,
        FileUploadModule,
        ProgressSpinnerModule,
        InputTextareaModule,
        DropdownModule,
        ButtonModule,
        CalendarModule,
        TooltipModule,
        TabViewModule,
        InputTextModule,
        InputMaskModule,
        InputSwitchModule,
        InputNumberModule,
        LoteRoutingModule,
        PanelModule,
        PasswordModule,
        TableModule,
        ChartModule,
        SelectButtonModule,
        AutoCompleteModule,
        ConfirmDialogModule,
        CoreModule,

        CustomComponetsModule
    ],
    declarations: [
        LoteComponent,
        PesquisaLoteComponent,
        DetalhesLoteComponent,
        InfoLoteComponent
    ],
    exports: [],
    providers: [
        LoteService
    ]
})
export class LoteModule {
}
