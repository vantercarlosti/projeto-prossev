import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoteComponent } from './lote.component';
import { PesquisaLoteComponent } from './pesquisa-lote/pesquisa-lote.component';
import { DetalhesLoteComponent } from './detalhes-lote/detalhes-lote.component';
import { InfoLoteComponent } from './info-lote/info-lote.component';

const routes: Routes = [
    { path: '', component: LoteComponent, children: [
        { path: 'cadastro', component: DetalhesLoteComponent },
        { path: 'pesquisa', component: PesquisaLoteComponent },
        { path: ':id', component: DetalhesLoteComponent },
    ]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoteRoutingModule {
}
