import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BreadcrumbService } from 'src/app/core/template/breadcrumb/breadcrumb.service';
import { LoteService } from 'src/app/shared/services/lote.service';
import { Lote } from 'src/app/shared/models/lote';

@Component({
  templateUrl: './pesquisa-lote.component.html',
  styleUrls: ['./pesquisa-lote.component.scss']
})
export class PesquisaLoteComponent implements OnInit {
  loading = false;
  filtro = new Lote();
  lotes = [];
  formulario: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loteService: LoteService,
    private formBuilder: FormBuilder,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Lote', routerLink: ['/lote/pesquisa'] },
      { label: 'Pesquisa' }
    ]);
  }

  ngOnInit(): void {
    this.configurarFormulario();
  }

  configurarFormulario() {
    this.formulario = this.formBuilder.group({
      id: [],
      lote: [ null, Validators.required ],
    });
  }

  consultar() {
    this.loteService.listar(this.filtro)
        .subscribe(resposta => {
            this.lotes = resposta.map(lote => new Lote(lote))
            console.log(this.lotes)
        })
  };

}
