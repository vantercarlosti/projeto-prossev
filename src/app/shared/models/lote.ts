export class Lote {
  id: string;
  lote: number;
  codigo: string;
  ean: string;
  nome: string;
  data_validade: Date;
  quantidade: number;
  quantidade_contada: number;
  expedicao: boolean = false;

  constructor(
    obj?: {
      id?: string;
      lote?: number;
      codigo?: string;
      ean?: string;
      nome?: string;
      data_validade?: Date;
      quantidade?: number;
      quantidade_contada?: number;
      expedicao?: any,
    }
  ) {
    if (obj != null) {
      this.id = obj.id;
      this.lote = obj.lote;
      this.codigo = obj.codigo;
      this.ean = obj.ean;
      this.nome = obj.nome;
      this.data_validade = obj.data_validade;
      this.quantidade = obj.quantidade;
      this.quantidade_contada = obj.quantidade_contada;
      this.expedicao = obj.expedicao || false;
    }
  }
}