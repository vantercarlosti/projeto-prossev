import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { Lote } from '../models/lote';

export class LancamentoFiltro {
  lote: number;
  data_validade: Date;
}

@Injectable({
  providedIn: 'root'
})
export class LoteService {

  loteUrl: string;

  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { 

    this.loteUrl = `${environment.apiUrl}/lotes`;

  }

  listar(filtro: LancamentoFiltro): Observable<any> {

    let params = new HttpParams()

    if (filtro.lote) {
      params = params.set('lote', filtro.lote.toString());
    }

    if (filtro.data_validade) {
      params = params.set('dataValidade',
        moment(filtro.data_validade).format('YYYY-MM-DD'));
    }

    return this.httpClient.get<Lote[]>(this.loteUrl, { params });
  }

  postLote(lote: any): Promise<any> {
    return this.httpClient.post(this.loteUrl, lote).toPromise();
  }

  putLote(lote: any): Promise<any> {
    return this.httpClient.put(`${this.loteUrl}/${lote.id}`, lote).toPromise();
  }

  buscarPorCodigo(id: number): Promise<Lote> {
    return this.httpClient.get<Lote>(`${this.loteUrl}/${id}`)
      .toPromise()
      .then(response => {
        const lote = response;

        this.converterStringsParaDatas([lote]);

        return lote;
      });
  }

  private converterStringsParaDatas(lotes: Lote[]) {
    for (const lote of lotes) {
      lote.data_validade = moment(lote.data_validade,
        'YYYY-MM-DD').toDate();

   
    }
  }

  
}
